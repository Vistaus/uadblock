import QtQuick 2.9
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import UAdBlock 1.0

Page {
    id: blockPage
    header: PageHeader {
        title: i18n.tr("Blocklists")
        trailingActionBar {
         actions: [
          Action {
            iconName: (settings.stevenBlockUnified && settings.stevenBlockUnifiedPorno && settings.energizedSocial && settings.energizedBluGo && settings.energizedSpark && settings.energizedXtreme && settings.goodbyeAds && settings.mvps && settings.adaway && settings.danpollocks && settings.peterlowes && settings.socialhosts && settings.coinblocker  && settings.youtubeAdBlock) ? "select-none" : "select"
            text: "select all"

            onTriggered: {
              if(!settings.stevenBlockUnified && !settings.stevenBlockUnifiedPorno && !settings.energizedSocial && !settings.energizedBluGo && !settings.energizedSpark && !settings.energizedXtreme && !settings.goodbyeAds && !settings.mvps && !settings.adaway && !settings.danpollocks && !settings.peterlowes && !settings.socialhosts && !settings.coinblocker  && !settings.youtubeAdBlock)
              {
                settings.stevenBlockUnified = true
                settings.stevenBlockUnifiedPorno = true
                settings.energizedSocial = true
                settings.energizedBluGo = true
                settings.energizedSpark = true
                settings.energizedXtreme = true
                settings.goodbyeAds = true
                settings.mvps = true
                settings.adaway = true
                settings.danpollocks = true
                settings.peterlowes = true
                settings.socialhosts = true
                settings.coinblocker = true
                settings.youtubeAdBlock = true
                settings.changes = true
              }
              else {
                settings.stevenBlockUnified = false
                settings.stevenBlockUnifiedPorno = false
                settings.energizedSocial = false
                settings.energizedBluGo = false
                settings.energizedSpark = false
                settings.energizedXtreme = false
                settings.goodbyeAds = false
                settings.mvps = false
                settings.adaway = false
                settings.danpollocks = false
                settings.peterlowes = false
                settings.socialhosts = false
                settings.coinblocker = false
                settings.youtubeAdBlock = false
                settings.changes = true
              }
            }
          }
          ]
        }
    }

    Flickable {
        anchors.fill: parent
        visible: !aIndicator.visible
        contentHeight: configuration.childrenRect.height

        Column {
            id: configuration
            anchors.fill: parent

            ListItem.SingleValue {
            }
            ListItem.Standard {
                text: "StevenBlack Unified"
                control: CheckBox {
                    id: enablestevenBlockUnified
                    checked: settings.stevenBlockUnified
                    onClicked: {
                        settings.changes = true
                        if(settings.stevenBlockUnified)
                            settings.stevenBlockUnified = false
                        else
                            settings.stevenBlockUnified = true
                    }
                }
            }
            ListItem.Standard {
                text: "StevenBlack Unified + Porno"
                control: CheckBox {
                    id: enablestevenBlockUnifiedPorno
                    checked: settings.stevenBlockUnifiedPorno
                    onClicked: {
                        settings.changes = true
                        if(settings.stevenBlockUnifiedPorno)
                            settings.stevenBlockUnifiedPorno = false
                        else
                            settings.stevenBlockUnifiedPorno = true
                    }
                }
            }
            ListItem.Standard {
                text: "StevenBlack Social Hosts"
                control: CheckBox {
                    id: enablesocialhosts
                    checked: settings.socialhosts
                    onClicked: {
                        settings.changes = true
                        if(settings.socialhosts)
                            settings.socialhosts = false
                        else
                            settings.socialhosts = true
                    }
                }
            }
            ListItem.Standard {
                text: "Energized Social"
                control: CheckBox {
                    id: enableenergizedSocial
                    checked: settings.energizedSocial
                    onClicked: {
                        settings.changes = true
                        if(settings.energizedSocial)
                            settings.energizedSocial = false
                        else
                            settings.energizedSocial = true
                    }
                }
            }
            ListItem.Standard {
                text: "Energized Blu Go"
                control: CheckBox {
                    id: enableenergizedBluGo
                    checked: settings.energizedBluGo
                    onClicked: {
                        settings.changes = true
                        if(settings.energizedBluGo)
                            settings.energizedBluGo = false
                        else
                            settings.energizedBluGo = true
                    }
                }
            }
            ListItem.Standard {
                text: "Energized Spark"
                control: CheckBox {
                    id: enableenergizedSpark
                    checked: settings.energizedSpark
                    onClicked: {
                        settings.changes = true
                        if(settings.energizedSpark)
                            settings.energizedSpark = false
                        else
                            settings.energizedSpark = true
                    }
                }
            }
            ListItem.Standard {
                text: "Energized Xtreme"
                control: CheckBox {
                    id: enableenergizedXtreme
                    checked: settings.energizedXtreme
                    onClicked: {
                        settings.changes = true
                        if(settings.energizedXtreme)
                            settings.energizedXtreme = false
                        else
                            settings.energizedXtreme = true
                    }
                }
            }
            ListItem.Standard {
                text: "GoodBye Ads by Jerryn70"
                control: CheckBox {
                    id: enablegoodbyeAds
                    checked: settings.goodbyeAds
                    onClicked: {
                        settings.changes = true
                        if(settings.goodbyeAds)
                            settings.goodbyeAds = false
                        else
                            settings.goodbyeAds = true
                    }
                }
            }
            ListItem.Standard {
                text: "YouTube AdBlocker by Jerryn70"
                control: CheckBox {
                    id: enableyoutubeAdBlock
                    checked: settings.youtubeAdBlock
                    onClicked: {
                        settings.changes = true
                        if(settings.youtubeAdBlock)
                            settings.youtubeAdBlock = false
                        else
                            settings.youtubeAdBlock = true
                    }
                }
            }
            ListItem.Standard {
                text: "MVPS"
                control: CheckBox {
                    id: enablemvps
                    checked: settings.mvps
                    onClicked: {
                        settings.changes = true
                        if(settings.mvps)
                            settings.mvps = false
                        else
                            settings.mvps = true
                    }
                }
            }
            ListItem.Standard {
                text: "Adaway"
                control: CheckBox {
                    id: enableadaway
                    checked: settings.adaway
                    onClicked: {
                        settings.changes = true
                        if(settings.adaway)
                            settings.adaway = false
                        else
                            settings.adaway = true
                    }
                }
            }
            ListItem.Standard {
                text: "dan pollock's hosts"
                control: CheckBox {
                    id: enabledanpollocks
                    checked: settings.danpollocks
                    onClicked: {
                        settings.changes = true
                        if(settings.danpollocks)
                            settings.danpollocks = false
                        else
                            settings.danpollocks = true
                    }
                }
            }
            ListItem.Standard {
                text: "Peter Lowe hosts"
                control: CheckBox {
                    id: enablepeterlowes
                    checked: settings.peterlowes
                    onClicked: {
                        settings.changes = true
                        if(settings.peterlowes)
                            settings.peterlowes = false
                        else
                            settings.peterlowes = true
                    }
                }
            }
            ListItem.Standard {
                text: "CoinBlockLists - by ZeroDot1"
                control: CheckBox {
                    id: enablecoinblocker
                    checked: settings.coinblocker
                    onClicked: {
                        settings.changes = true
                        if(settings.coinblocker)
                            settings.coinblocker = false
                        else
                            settings.coinblocker = true
                    }
                }
            }
        }
    }
}
